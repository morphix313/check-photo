import json
import logging
import os
import sys
import pika
import time
import requests

from deepface import DeepFace


logging.basicConfig(
    # filename="/var/log/worker/worker.log",
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.StreamHandler(sys.stdout)],
)
_logger = logging.getLogger(__name__)


class Worker:
    host = os.environ.get('RABBITMQ_HOST', "localhost")
    queue = os.environ.get('RABBITMQ_QUEUE', "hello-queue")

    @classmethod
    def start(cls):
        credentials = pika.PlainCredentials(
            os.environ.get('RABBITMQ_DEFAULT_USER'), os.environ.get('RABBITMQ_DEFAULT_PASS'))
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=cls.host,
                port=5672,
                credentials=credentials,
            )
        )
        channel = connection.channel()
        channel.queue_declare(queue=cls.queue, durable=True)

        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(queue=cls.queue, on_message_callback=cls.callback)

        channel.start_consuming()

        _logger.info(' [*] Waiting for messages. To exit press CTRL+C')

    @classmethod
    def callback(cls, ch, method, properties, body):
        message = json.loads(body)
        _logger.info(" [x] Received %r" % message)

        status = 'in_progress'
        cls.change_status(message['user_id'], status)

        _logger.info(" [x] Processing...")
        time.sleep(3)

        if cls.validate_photo(message['file']):
            status = 'verified'
            cls.save_photo(message['user_id'], message['file'])
        else:
            status = 'error'

        cls.change_status(message['user_id'], status)
        _logger.info(f" [x] Status: {status}")

        _logger.info(" [x] Processed")
        ch.basic_ack(delivery_tag=method.delivery_tag)

    @classmethod
    def validate_photo(cls, photo):
        _logger.info(f" [x] Validete photo: {photo}")

        # Face detection
        try:
            face_objs = DeepFace.extract_faces(
                img_path=photo,
                target_size=(500, 500),
                detector_backend='retinaface',
            )
        except ValueError as e:
            _logger.info(f"Error: {e}")
            return False
        else:
            if len(face_objs) > 0:
                return True
            return False

    @classmethod
    def change_status(cls, user_id, status):
        res = requests.put(
            f"http://api:8000/api/employees/{user_id}/status/",
            json={"status": status}
        )
        res.raise_for_status()

        _logger.info(f" [x] Status changed: {status}")

    @classmethod
    def save_photo(cls, user_id, photo):
        res = requests.put(
            f"http://api:8000/api/employees/{user_id}/photo/",
            files={'photo': open(photo, 'rb')}
        )
        res.raise_for_status()

        _logger.info("[x] Photo saved")


if __name__ == '__main__':
    _logger.info("[x] Start worker...")
    Worker.start()

import json
import os
import pika

from django.apps import AppConfig
from django.conf import settings as django_settings


class EmployeesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'employees'


class PhotoValidator:
    host = os.environ.get('RABBITMQ_HOST', "localhost")
    queue = os.environ.get('RABBITMQ_QUEUE', "check-photo")

    @classmethod
    def validate(cls, user_id, photo):
        file_path = cls.save_photo(user_id, photo)
        cls.send_to_worker(user_id, file_path)

    @classmethod
    def save_photo(cls, user_id, photo):
        dir_path = (django_settings.MEDIA_ROOT / 'temp' / str(user_id)).resolve()
        file_path = (dir_path / photo._name).resolve()

        # Save photo
        if not os.path.exists(os.path.dirname(file_path)):
            os.makedirs(os.path.dirname(file_path))

        with open(file_path, "wb") as f:
            f.write(photo.read())

        return str(file_path)

    @classmethod
    def send_to_worker(cls, user_id, file_path):
        credentials = pika.PlainCredentials(
            os.environ.get('RABBITMQ_DEFAULT_USER'), os.environ.get('RABBITMQ_DEFAULT_PASS'))
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=cls.host,
                port=5672,
                credentials=credentials,
            )
        )

        channel = connection.channel()
        channel.queue_declare(queue=cls.queue, durable=True)

        message = {'user_id': user_id, 'file': file_path}

        channel.basic_publish(
            exchange='',
            routing_key=cls.queue,
            body=json.dumps(message),
            properties=pika.BasicProperties(delivery_mode=2)
        )

        print(" [x] Sent %r" % message)
        connection.close()

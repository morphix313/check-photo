from django.db import models


class Employee(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100, blank=True, default='')
    email_address = models.EmailField()
    company_name = models.CharField(max_length=100, blank=True, default='')
    photo = models.ImageField(upload_to='employees/', blank=True, null=True)
    status_items = [
        ('new', 'New'),
        ('in_progress', 'In Progress'),
        ('verified', 'Verified'),
        ('error', 'Error'),
    ]
    status = models.CharField(max_length=20, choices=status_items, default='new')

    def __str__(self):
        return f'{self.id}: {self.first_name} {self.last_name}'

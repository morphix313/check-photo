from django.test import TestCase
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient

from employees.models import Employee


class EmployeeAPITest(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_list_employees(self):
        url = reverse('employee-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_employee(self):
        url = reverse('employee-list')
        data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'email_address': 'john.doe@example.com',
            'company_name': 'Example Company',
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_retrieve_employee(self):
        employee = Employee.objects.create(
            first_name='John',
            last_name='Doe',
            email_address='john.doe@example.com',
            company_name='Example Company',
        )
        url = reverse('employee-detail', args=[employee.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_employee(self):
        employee = Employee.objects.create(
            first_name='John',
            last_name='Doe',
            email_address='john.doe@example.com',
            company_name='Example Company',
        )
        url = reverse('employee-detail', args=[employee.pk])
        data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email_address': 'john.smith@example.com',
            'company_name': 'Updated Company',
        }
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_employee(self):
        employee = Employee.objects.create(
            first_name='John',
            last_name='Doe',
            email_address='john.doe@example.com',
            company_name='Example Company',
        )
        url = reverse('employee-detail', args=[employee.pk])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_update_employee_status(self):
        employee = Employee.objects.create(
            first_name='John',
            last_name='Doe',
            email_address='john.doe@example.com',
            company_name='Example Company',
        )
        url = reverse('employee-update-status', args=[employee.pk])
        data = {
            'status': 'in_progress',
        }
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

from rest_framework.serializers import ModelSerializer

from employees.models import Employee


class EmployeeSerializer(ModelSerializer):
    class Meta:
        model = Employee
        # fields = '__all__'
        exclude = ['photo', 'status']


class EmployeeUpdatePhotoSerializer(ModelSerializer):
    class Meta:
        model = Employee
        fields = ['photo']


class EmployeeUpdateStatusSerializer(ModelSerializer):
    class Meta:
        model = Employee
        fields = ['status']

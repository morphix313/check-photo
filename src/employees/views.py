import json

from rest_framework.viewsets import ViewSet
from rest_framework.response import Response

from employees.apps import PhotoValidator
from employees.models import Employee
from employees.serializers import (
    EmployeeSerializer,
    EmployeeUpdatePhotoSerializer,
    EmployeeUpdateStatusSerializer,
)


class EmployeeViewSet(ViewSet):
    def list(self, request, *args, **kwargs):
        employees = Employee.objects.all()
        serializer = EmployeeSerializer(employees, many=True)
        return Response(serializer.data, status=200)

    def create(self, request, *args, **kwargs):
        data = request.data

        serialaizer = EmployeeSerializer(data=data)

        if not serialaizer.is_valid():
            return Response(json.dumps({'message': "Invalid data"}), status=400)

        employee = serialaizer.save()

        if photo := data.get('photo'):
            PhotoValidator.validate(employee.id, photo)

        return Response(serialaizer.data, status=201)


class EmployeeDetailView(ViewSet):
    def retrieve(self, request, *args, **kwargs):
        employee = Employee.objects.get(id=kwargs['pk'])
        serializer = EmployeeSerializer(employee)
        return Response(serializer.data, status=200)

    def update(self, request, *args, **kwargs):
        data = request.data

        employee = Employee.objects.get(id=kwargs['pk'])
        serializer = EmployeeSerializer(data=data)

        if not serializer.is_valid():
            return Response(json.dumps({'message': "Invalid data"}), status=400)

        serializer.update(employee, serializer.validated_data)

        if photo := data.get('photo'):
            PhotoValidator.validate(employee.id, photo)

        return Response(serializer.data, status=200)

    def destroy(self, request, *args, **kwargs):
        employee = Employee.objects.get(id=kwargs['pk'])
        employee.delete()

        return Response(status=204)


class EmployeeUpdatePhotoView(ViewSet):
    def update(self, request, *args, **kwargs):
        employee = Employee.objects.get(id=kwargs['pk'])

        serializer = EmployeeUpdatePhotoSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(json.dumps({'message': "Invalid data"}), status=400)

        serializer.update(employee, serializer.validated_data)
        return Response(serializer.data, status=200)


class EmployeeUpdateStatusView(ViewSet):
    def update(self, request, *args, **kwargs):
        employee = Employee.objects.get(id=kwargs['pk'])

        serializer = EmployeeUpdateStatusSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(json.dumps({'message': "Invalid data"}), status=400)

        serializer.update(employee, serializer.validated_data)
        return Response(serializer.data, status=200)

"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from employees.views import (
    EmployeeDetailView,
    EmployeeViewSet,
    EmployeeUpdatePhotoView,
    EmployeeUpdateStatusView
)


employees = EmployeeViewSet.as_view({'get': 'list', 'post': 'create'})
employee_detail = EmployeeDetailView.as_view(
    {'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})
employee_update_photo = EmployeeUpdatePhotoView.as_view({'put': 'update'})
employee_update_status = EmployeeUpdateStatusView.as_view({'put': 'update'})

urlpatterns = format_suffix_patterns([
    path('admin/', admin.site.urls),
    path('api/employees/', employees, name='employee-list'),
    path('api/employees/<int:pk>/', employee_detail, name='employee-detail'),
    path('api/employees/<int:pk>/photo/', employee_update_photo, name='employee-update-photo'),
    path('api/employees/<int:pk>/status/', employee_update_status, name='employee-update-status'),
])

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

## Setup

build and up containers

```
docker-compose up
```

run migrations

```
docker-compose run web python3 manage.py migrate
```

create a user for admin

```
docker-compose run web python manage.py createsuperuser
```


## API:
  * Get list objects:
      - localhost:8001/employees
      - HTTP method: GET

  * Get object (retrieve):
      - localhost:8001/employees/2/
      - HTTP method: GET
  
  * Create object:
      - localhost:8001/employees/
      - HTTP method: POST
      - example body: {"first_name": "John", "email_address": "john@test.com"}

  * Update object:
      - localhost:8001/employees/2/
      - HTTP method: PUT
      - example body: {"first_name": "John-2", "email_address": "john-2@test.com"}
  
  * Delete object:
      - localhost:8001/employees/2/
      - HTTP method: DELETE
